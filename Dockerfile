FROM openjdk:8
MAINTAINER gerold (dgkondengui@gmail.com)
ADD ./build/libs/cars-api.jar cars-api.jar
EXPOSE 5000
ENTRYPOINT ["java","-jar","cars-api.jar"]
